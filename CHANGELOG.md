# Change Log

### 0.9.1

2019-11-17

- Fixed Distinct colorization only actually working when options page was shown
- Fixed settings being applied muliple times incorrectly, including old settings 

### 0.9

2019-11-10

- Initial public version
