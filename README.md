# Distinct web extension

Distinct is a browser extension that applies themes to site whose URLs 
match patterns to give them a distinct look in the browser.
If you are a developer, this add-on can be used to easily identify dev, 
test and production environments by turning the browser particular 
colors when their tabs are active.

## Why I wrote this

As a developer, I work with sites that are at various stages of 
development - dev, test and finally production.  The URLs often 
look similar, and the sites themselves definitely look similar.
Sometimes, I ended up accidentally adding test data to production 
environments because I forgot which environment I was in.

What I wanted was an easy way to make it dead-obvious if I was in 
a production environment, so this extension was born.  

By adding the patterns for the production systems I am working with 
in the Distinct extension configuration, when the browser goes
bright red I then realize I'm in a production environment.  No more 
mistakes!

## Usage

After installing, go the the extension's options page and press the 
'+' button to add a site pattern.  On the new row, set the pattern
to a string that matches the website URL.  The pattern by default does 
a contains match so that if the string you enter is contained in the URL
then it is considered a match.  For example if you want
the browser to turn red for 'example.com', you can enter 'example.com'
as the pattern.  However, if you want it to turn red for 'example.com' 
but green for 'test.example.com', it would be better to match against
`http://example.com` and `http://test.example.com`.

The 'translucent page overlay amount' controls how much of a tint
the entire page gets (as opposed to the browser theme which is the rest
of the browser controls).  This can be disabled by using a value of 0%.  

## Building

To build the extension, run `npm run package` from the `extension`
directory.  A ZIP file containing the extension will be built in the 
`web-ext-artifacts` sub-directory.

## Developing

The extension is written in Typescript and is translated when built.
When developing, you can use `npm run runwatch` which will:

- Run webpack and rebuild code every time the source changes
- Run Firefox with the extension and automatically reload it when code
changes
