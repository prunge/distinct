import {Storage} from 'webextension-polyfill-ts';
import {DistinctConfiguration} from './config';
import StorageArea = Storage.StorageArea;

export interface ConfigurationRepository {
  load(): Promise<DistinctConfiguration>;
  save(config: DistinctConfiguration): Promise<void>;
}

export class LocalStorageConfigurationRepository implements ConfigurationRepository {
    constructor(private readonly storage: StorageArea) {
    }

    async load(): Promise<DistinctConfiguration> {
        const rawConfig: Partial<DistinctConfiguration> = await this.storage.get();
        return {
            sites: rawConfig.sites ?? [],
            overlayAmount: rawConfig.overlayAmount ?? 0.25
        };
    }

    async save(config: DistinctConfiguration): Promise<void> {
        await this.storage.set(config);
    }
}
