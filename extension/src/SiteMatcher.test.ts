import {DistinctSite} from './config';
import {matcherForDistinctSite} from './SiteMatcher';

describe('simple patterns', () => {
    it('should match', () => {
        const site: DistinctSite = { urlPattern: 'google.com', color: [0, 0, 0] };
        const matcher = matcherForDistinctSite(site);
        expect(matcher.matches('https://www.google.com/something')).toBeTruthy();
    });

    it('should not match', () => {
        const site: DistinctSite = { urlPattern: 'google.com', color: [0, 0, 0] };
        const matcher = matcherForDistinctSite(site);
        expect(matcher.matches('https://www.galahsrus.com')).toBeFalsy();
    });
});

describe('regexp patterns', () => {
    it('should match', () => {
        const site: DistinctSite = { urlPattern: '/cat.*dog/', color: [0, 0, 0] };
        const matcher = matcherForDistinctSite(site);
        expect(matcher.matches('https://www.catdog.com/something')).toBeTruthy();
        expect(matcher.matches('https://www.catanddog.com/something')).toBeTruthy();
    });

    it('should not match', () => {
        const site: DistinctSite = { urlPattern: '/cat.*dog/', color: [0, 0, 0] };
        const matcher = matcherForDistinctSite(site);
        expect(matcher.matches('https://www.dogcat.com/something')).toBeFalsy();
    });
});

describe('empty patterns', () => {
    it('should never match anything', () => {
        const site: DistinctSite = { urlPattern: '', color: [0, 0, 0] };
        const matcher = matcherForDistinctSite(site);
        expect(matcher.matches('https://www.google.com')).toBeFalsy();
    });
});
