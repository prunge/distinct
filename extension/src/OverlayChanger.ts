import { AppearanceChanger, DistinctPage } from './AppearanceChanger';
import { DistinctSiteOverlay } from './config';
import { Tabs } from 'webextension-polyfill-ts';

export class OverlayChanger implements AppearanceChanger {

    constructor(private readonly tabs: Tabs.Static) {
    }

    async initialize(): Promise<void> {
    }

    private static overlayableUrl(url?: string): boolean {
        if (!url) {
            return false;
        }
        else if (url.startsWith('about:')) {
            return false;
        }

        return true;
    }

    async update(page: DistinctPage, pageAppearance?: DistinctSiteOverlay): Promise<void> {

      // Some pages cannot be modified, like about:*
      if (!OverlayChanger.overlayableUrl(page.url)) {
        return;
      }

      // Load up configuration for the content script
      await this.tabs.executeScript(page.tabId, {
        code: `distinct__site_configuration = ${JSON.stringify(pageAppearance)};`
      });

      // Run the content script that applies the overlay
      await this.tabs.executeScript(page.tabId, {
        file: '/dist/content_overlay.js'
      });
    }
}
