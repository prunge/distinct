import * as React from 'react';
import {colorToHexString, hexStringToColor} from './ColorUtils';
import {DistinctSite} from './config';

export interface SiteRowProps {
    site: DistinctSite;
    onChange?: (site: DistinctSite) => void;
    onRemove?: () => void;
}

export class SiteRow extends React.Component<SiteRowProps> {
    render() {
        return (
          <tr>
              <td>
                  <input type="color"
                         title="Color to use for the browser theme and page overlay"
                         value={colorToHexString(this.props.site.color)}
                         onChange={ev =>
                             this.props.onChange?.({...this.props.site, color: hexStringToColor(ev.target.value)})}
                  />
              </td>
              <td>
                  <input title="Site match pattern.  Uses simple string matching by default on the site URL.  Enclose pattern between '/' characters to use a regular expression instead."
                         value={this.props.site.urlPattern}
                         style={{width: '100%'}}
                         onChange={ev => this.props.onChange?.({...this.props.site, urlPattern: ev.target.value})}
                  />

              </td>
              <td>
                  {this.props.onRemove ?
                      <button title="Remove this row"
                              onClick={() => this.props.onRemove?.()}>
                          -
                      </button> : undefined
                  }
              </td>
          </tr>
        );
    }
}
