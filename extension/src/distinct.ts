import {Browser, Tabs, Windows} from 'webextension-polyfill-ts';
import {matcherForDistinctSite} from './SiteMatcher';
import Tab = Tabs.Tab;
import Window = Windows.Window;
import { AppearanceChanger } from './AppearanceChanger';
import { ThemeChanger } from './ThemeChanger';
import { OverlayChanger } from './OverlayChanger';
import { ConfigurationRepository } from './ConfigurationRepository';

export class Distinct {
    private readonly appearanceChangers: AppearanceChanger[];

    constructor(private readonly browser: Browser, private configRepository: ConfigurationRepository) {
        this.appearanceChangers = [
            new ThemeChanger(browser),
            new OverlayChanger(browser.tabs)
        ];

        browser.tabs.onActivated.addListener(async activeInfo => await this.tabIdUpdated(activeInfo.tabId));
        browser.tabs.onUpdated.addListener(async (tabId, changeInfo, tab) => await this.tabUpdated(tab));
        browser.windows.onFocusChanged.addListener(
            async windowId => {
                // -1 means no window is focused in Chrome
                if (windowId !== -1) {
                    await this.windowUpdated(await browser.windows.get(windowId, {populate: true}));
                }
            });
    }

    async initialize(): Promise<void> {
        for (const appearanceChanger of this.appearanceChangers) {
            await appearanceChanger.initialize();
        }
    }

    private async windowUpdated(window: Window): Promise<void> {
        const activeTabInWindow = window.tabs?.find(tab => tab.active);
        if (activeTabInWindow) {
            await this.tabUpdated(activeTabInWindow);
        }
    }

    private async tabIdUpdated(tabId: number): Promise<void> {
        const tab = await this.browser.tabs.get(tabId);
        await this.tabUpdated(tab);
    }

    private async tabUpdated(tab: Tab): Promise<void> {
        // Only active tabs update the theme
        if (tab.active && tab.windowId !== undefined && tab.id !== undefined) {
            await this.updateThemeForUrl(tab.windowId, tab.id, tab.url);
        }
    }

    private async updateThemeForUrl(windowId: number, tabId: number, url?: string): Promise<void> {
        console.log('Distinct: Tab was updated:', url);

        try {
            if (url) {
                const config = await this.configRepository.load();
                const matchingSite = config.sites.find(site => matcherForDistinctSite(site).matches(url));
                if (matchingSite) {
                    for (const appearanceChanger of this.appearanceChangers) {
                        await appearanceChanger.update({windowId, tabId, url}, {...matchingSite, overlayAmount: config.overlayAmount});
                    }
                    return;
                }
            }

            // If we get here, just reset the appearance
            for (const appearanceChanger of this.appearanceChangers) {
                await appearanceChanger.update({windowId, tabId, url});
            }
        } catch (err) {
            console.error('Distinct appearance change failed:', url, err);
        }
    }
}
