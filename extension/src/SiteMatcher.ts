import {DistinctSite} from './config';

export interface SiteMatcher {
    matches(pageUrl: string): boolean;
}

class RegExpSiteMatcher implements SiteMatcher {
    constructor(private readonly regExp: RegExp) {
    }

    matches(pageUrl: string): boolean {
        return this.regExp.test(pageUrl);
    }
}

class NoMatcher implements SiteMatcher {
    matches(pageUrl: string): boolean {
        return false;
    }
}

export function matcherForDistinctSite(site: DistinctSite): SiteMatcher {
    if (site.urlPattern.trim().length === 0) {
        return new NoMatcher();
    } else if (site.urlPattern.startsWith('/') && site.urlPattern.endsWith('/')) {
        return new RegExpSiteMatcher(new RegExp(site.urlPattern.substring(1, site.urlPattern.length - 1), 'i'));
    } else {
        return new RegExpSiteMatcher(new RegExp(escapeRegExp(site.urlPattern), 'i'));
    }
}

// See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions#Escaping
function escapeRegExp(s: string): string {
    return s.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}
