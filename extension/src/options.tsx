import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {distinctConfigRepo} from './background';
import {OptionsPane} from './OptionsPane';

ReactDOM.render(
    <OptionsPane repo={distinctConfigRepo} />,
    document.getElementById('distinct-options')
);
