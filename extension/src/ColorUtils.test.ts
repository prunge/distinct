import {colorToHexString, hexStringToColor} from './ColorUtils';
import {DistinctColorArray} from './config';

it('converts colors from array to hex', () => {
    const colorRed: DistinctColorArray = [255, 0, 0];
    const hexString = colorToHexString(colorRed);
    expect(hexString).toBe('#ff0000');
});

it('converts colors from hex to array', () => {
    const hexRed = '#ff0000';
    const color = hexStringToColor(hexRed);
    expect(color).toStrictEqual([255, 0, 0]);
});
