import {DistinctColorArray} from './config';

export function colorToHexString(color: DistinctColorArray): string {
    return '#' + color.map(c => colorComponentToHexString(c)).join('');
}

export function hexStringToColor(hexString: string): DistinctColorArray {
    // Assume first char is '#' and 6 characters afterwards
    if (!hexString.startsWith('#') || hexString.length < 7) {
        console.error('Error parsing hex string from color component:', hexString);
        return [0, 0, 0];
    }

    return [
        hexStringComponentToColor(hexString.substring(1, 3)),
        hexStringComponentToColor(hexString.substring(3, 5)),
        hexStringComponentToColor(hexString.substring(5, 7))
    ];
}

function colorComponentToHexString(colorComponent: number): string {
    return colorComponent.toString(16).padStart(2, '0');
}

function hexStringComponentToColor(hexComponent: string): number {
    return parseInt(hexComponent, 16);
}
