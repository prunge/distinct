import {browser} from 'webextension-polyfill-ts';
import { ConfigurationRepository, LocalStorageConfigurationRepository } from './ConfigurationRepository';
import {Distinct} from './distinct';

declare module 'webextension-polyfill-ts' {
  export namespace Extension {
    export interface Static {
      getBackgroundPage(): Window & DistinctBackgroundPage;
    }
  }
}

export interface DistinctBackgroundPage {
  distinctInstance?: Distinct;
}

export const distinctConfigRepo: ConfigurationRepository =
  new LocalStorageConfigurationRepository(browser.storage.sync ?? browser.storage.local);

async function distinct(): Promise<Distinct> {

  // This function may be called from non-background pages since it is exported
  let distinctInstance = browser.extension.getBackgroundPage().distinctInstance;
  // console.log('Instance from bg page: ', browser.extension.getBackgroundPage().distinctInstance);

  // Use existing instance if is already initialized
  // otherwise make a new one
  if (!distinctInstance) {
    // Some browser types (e.g. Firefox mobile) does not have sync storage type so fallback to local
    // Can happen if distinct instance is half initialized
    // Not fully initialized but has the config so should be OK
    if (distinctInstance) {
      return distinctInstance;
    }

    distinctInstance = new Distinct(browser, distinctConfigRepo);
    browser.extension.getBackgroundPage().distinctInstance = distinctInstance;

    await distinctInstance.initialize();
    console.debug('Distinct fully initialized: ', distinctInstance);
  }

  return distinctInstance;
}

async function initialize(): Promise<void> {
  // Initialize the distinct instance now
  await distinct();
}

initialize().catch(err => console.log('Error loading extension:', err));
