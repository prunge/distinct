import {DistinctSiteOverlay} from './config';
declare const distinct__site_configuration: DistinctSiteOverlay | undefined;

const DISTINCT_OVERLAY_ID = 'distinct-extension-overlay';

function applyOverlay(config?: DistinctSiteOverlay): void {
    console.debug('Applying overlay:', JSON.stringify(config));

    const existingOverlayDiv = document.getElementById(DISTINCT_OVERLAY_ID);
    if (existingOverlayDiv) {
        document.body.removeChild(existingOverlayDiv);
    }

    if (config) {
        const overlayDiv = document.createElement('div');
        overlayDiv.id = DISTINCT_OVERLAY_ID;
        overlayDiv.style.position = 'fixed';
        overlayDiv.style.width = '100%';
        overlayDiv.style.height = '100%';
        overlayDiv.style.top = '0';
        overlayDiv.style.left = '0';
        overlayDiv.style.bottom = '0';
        overlayDiv.style.right = '0';
        overlayDiv.style.zIndex = '1000000'; // Enough to be over the top of everything
        overlayDiv.style.pointerEvents = 'none'; // Allow click through
        overlayDiv.style.backgroundColor = `rgba(${config.color[0]}, ${config.color[1]}, ${config.color[2]}, ${config.overlayAmount})`;
        document.body.appendChild(overlayDiv);
    }
}

applyOverlay(distinct__site_configuration);
