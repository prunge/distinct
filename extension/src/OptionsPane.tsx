import * as React from 'react';
import {DistinctConfiguration, DistinctSite} from './config';
import {ConfigurationRepository} from './ConfigurationRepository';
import {SiteRow} from './SiteRow';

export interface OptionsPaneProps {
    repo: ConfigurationRepository;
    onChange?: (config: DistinctConfiguration) => void;
}

export interface OptionsPaneState {
    config: DistinctConfiguration;
}

export class OptionsPane extends React.Component<OptionsPaneProps, OptionsPaneState> {

    private readonly percentFormat = new Intl.NumberFormat(undefined, {style: 'percent'});

    state: Readonly<OptionsPaneState> = {
        config: {
            overlayAmount: 0.0,
            sites: []
        }
    };

    componentDidMount(): void {
        this.props.repo.load().then(config => this.setState({config}))
                              .catch(err => console.log('Error setting options pane state: ', err));
    }

    private async updateConfig(configChange: Partial<DistinctConfiguration>): Promise<void> {
        const newFullConfig: DistinctConfiguration = {...this.state.config, ...configChange};
        this.setState({config: newFullConfig});
        await this.props.repo.save(newFullConfig);
        this.props.onChange?.(newFullConfig);
        console.debug('Update distinct config to: ', newFullConfig);
    }

    private async updateSite(siteChange: Partial<DistinctSite>, siteIndex: number): Promise<void> {
        const newSite = { ...this.state.config.sites[siteIndex], ...siteChange };
        const newSites = [...this.state.config.sites];
        newSites[siteIndex] = newSite;
        await this.updateConfig({sites: newSites});
    }

    private async removeSite(siteIndex: number): Promise<void> {
        const newSites = [...this.state.config.sites];
        newSites.splice(siteIndex, 1);
        await this.updateConfig({sites: newSites});
    }

    render() {
        return (
            <div>
                <table id="siteTable" style={{width: '100%'}}>
                    <colgroup>
                        <col />
                        <col style={{width: '100%'}} />
                        <col />
                    </colgroup>
                    <thead>
                        <tr>
                            <td>Color</td>
                            <td>Site match pattern</td>
                            <td>
                                <button title="Add a new pattern row"
                                        onClick={() => this.updateConfig({sites: [...this.state.config.sites,
                                                                                    {color: [255, 0, 0], urlPattern: ''}
                                                                                  ]})}>
                                    +
                                </button>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.config.sites.map((site, siteIndex) =>
                            <SiteRow site={site}
                                     onChange={async newSite => await this.updateSite(newSite, siteIndex)}
                                     onRemove={async () => await this.removeSite(siteIndex)}
                                     key={siteIndex}
                            />)}
                    </tbody>
                </table>

                <div style={{display: 'flex'}}>
                    <label htmlFor="overlayAmount">Translucent page overlay amount</label>
                    <input id="overlayAmount" type="range" min="0.0" max="100.0"
                           value={this.state.config.overlayAmount * 100.0}
                           onChange={async ev =>
                               await this.updateConfig({overlayAmount: Number(ev.target.value) / 100.0})}
                           title="As well as the browser controls, the page itself gets a translucent colored overlay with this strength.  Make this value zero to disable page overlays."
                           style={{flexGrow: 1}} />
                    <span id="overlayAmountInfo" style={{width: '9em'}}>
                        {this.state.config.overlayAmount > 0
                            ? this.percentFormat.format(this.state.config.overlayAmount)
                            : 'disabled'}
                    </span>
                </div>
            </div>
        );
    }
}
