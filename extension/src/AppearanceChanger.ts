import { DistinctSiteOverlay } from './config';

export interface AppearanceChanger {
    initialize(): Promise<void>;
    update(page: DistinctPage, pageAppearance?: DistinctSiteOverlay): Promise<void>;
}

export interface DistinctPage {
  readonly windowId: number;
  readonly tabId: number;
  readonly url?: string;
}
