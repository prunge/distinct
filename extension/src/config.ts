export interface DistinctConfiguration {
    sites: DistinctSite[];
    overlayAmount: number;
}

export interface DistinctSite {
    urlPattern: string;
    color: DistinctColorArray;
}

export type DistinctSiteOverlay = DistinctSite & Pick<DistinctConfiguration, 'overlayAmount'>;

/**
 * RGB color, with elements [red, green, blue] each with values 0-255.
 */
export type DistinctColorArray = [number, number, number];
