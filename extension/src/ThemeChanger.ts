import { AppearanceChanger, DistinctPage } from './AppearanceChanger';
import { Browser, Manifest, Windows } from 'webextension-polyfill-ts';
import { DistinctSiteOverlay } from './config';
import ThemeType = Manifest.ThemeType;
import Window = Windows.Window;

export class ThemeChanger implements AppearanceChanger {
    private windowBrowserThemes: {[windowId: number]: ThemeType} = {};

    constructor(private readonly browser: Browser) {
        this.browser.windows.onCreated.addListener(async window => this.recordWindowOriginalTheme(window));
        this.browser.windows.onRemoved.addListener(windowId => delete this.windowBrowserThemes[windowId]);
    }

    async initialize(): Promise<void> {
        const windows = await this.browser.windows.getAll({});
        windows.forEach(window => this.recordWindowOriginalTheme(window));
    }

    private async recordWindowOriginalTheme(window: Window): Promise<void> {
        this.windowBrowserThemes[window.id || -1] = await this.browser.theme?.getCurrent(window.id);
    }

    async update(page: DistinctPage, pageAppearance?: DistinctSiteOverlay): Promise<void> {
        if (pageAppearance) {
            console.debug('Set theme color:', pageAppearance.color);

            await this.browser.theme?.update(page.windowId, {
                colors: {
                    frame: pageAppearance.color
                }
            });
        } else {
            console.debug('Revert theme to default.');
            // Seems to cause more problems than it solves...
            // causes a flash of the window briefly after every tab/window focus change, yuck
            // reset just seems useless for now
            // await this.browser.theme?.reset(windowId);

            const originalTheme = this.windowBrowserThemes[page.windowId];
            if (originalTheme) {
                console.debug('...and apply original theme', originalTheme);
                await this.browser.theme?.update(page.windowId, originalTheme);
            } else {
                console.debug('...and no original theme to apply');
            }
        }
    }
}
