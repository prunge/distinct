module.exports = {
    mode: "development",
    devtool: "inline-source-map",
    entry: {
        background: './src/background.tsx',
        content_overlay: './src/content_overlay.tsx',
        options: './src/options.tsx',
        "edge/backgroundScriptsAPIBridge": './src/edge/backgroundScriptsAPIBridge.js',
        "edge/contentScriptsAPIBridge": './src/edge/backgroundScriptsAPIBridge.js'
    },
    output: {
        path: __dirname + "/dist",
        filename: '[name].js',
    },
    resolve: {
        // Add `.ts` and `.tsx` as a resolvable extension.
        extensions: [".ts", ".tsx", ".js"]
    },
    module: {
        rules: [
            // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
            { test: /\.tsx?$/, loader: "ts-loader" }
        ]
    },
    watchOptions: {
        // This delay allows Firefox web-ext run enough time to not pick up partial changes and reload full changes
        aggregateTimeout: 1000
    }
};
